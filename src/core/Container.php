<?php
namespace App\core;

/**
 * Class Container
 * @package App\core
 */
class Container
{
    /**
     * @var
     */
    private static $instance;


    /**
     * @return $this
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }
    /**
     * @var
     */
    private $results = [];
    /**
     * @var
     */
    private $definitions = [];

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        if(array_key_exists($id, $this->results)){
            return $this->results[$id];
        }

        if (!array_key_exists($id, $this->definitions)) {
            throw new \Exception();
        }

        $definition = $this->definitions[$id];

        if ($definition instanceof \Closure) {
            $this->results[$id] = $definition($this);
        }else{
            $this->results[$id] = $definition;
        }
        return $this->results[$id];
    }

    /**
     * @param $id
     * @param $value
     */
    public function set($id, $value)
    {
        $this->definitions[$id] = $value;
    }
}