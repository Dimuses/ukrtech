<?php

namespace App;

use App\core\Container;
use PDO;

/**
 * Class Query
 * @package App
 */
class Query
{
    /**
     * @var PDO
     */
    private $db;

    /**
     * @var
     */
    private $from;

    /**
     * @var
     */
    private $where;

    /**
     * @var
     */
    private $order;

    /**
     * @var
     */
    private $limit;

    /**
     * @var
     */
    private $params;

    /**
     * @return mixed
     */
    protected function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    protected function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    protected function getWhere()
    {
        return $this->where;
    }

    /**
     * @param mixed $where
     */
    protected function setWhere($where)
    {
        $this->where[] = $where;
    }

    /**
     * @return mixed
     */
    protected function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    protected function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    protected function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param mixed $limit
     */
    protected function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * Query constructor.
     */
    public function __construct()
    {
        try {
            $this->db = Container::getInstance()->get('db');
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        return $this;
    }

    /**
     * @param string $table
     * @return $this
     */
    public function from(string $table)
    {
        $this->setFrom(trim($table));
        return $this;
    }

    /**
     * @param $condition
     * @param array $values
     * @return $this
     */
    public function where($condition, array $values = [])
    {
        if (is_array($condition)) {
            foreach ($condition as $index => $item) {
                $this->setWhere($item);
            }
        } else {
            $this->setWhere($condition);
        }
        $this->addParams($values);
        return $this;
    }

    /**
     * @param $field
     * @param $direction
     * @return $this
     */
    public function order($field, $direction = 'ASC')
    {
        $direction = strtoupper($direction);

        if (in_array($direction, ['ASC', 'DESC'])) {
            $direction = 'ASC';
        }
        $this->setOrder([
            trim($field) => $direction
        ]);
        return $this;
    }

    /**
     * @param int $limit
     * @return Query
     */
    public function limit(int $limit)
    {
        $this->setLimit($limit);
        return $this;
    }

    /**
     * @param $params
     * @return $this
     */
    protected function addParams($params)
    {
        if (!empty($params)) {
            if (empty($this->params)) {
                $this->params = $params;
            } else {
                foreach ($params as $name => $value) {
                    if (is_int($name)) {
                        $this->params[] = $value;
                    } else {
                        $this->params[$name] = $value;
                    }
                }
            }
        }
        return $this;
    }

    protected function getParam($param)
    {
        $var = $this->params[$param];
        if (isset($var)) {
            return $this->db->quote($var);
        }
        return null;
    }

    public function __toString()
    {
        $from = $this->buildFrom();
        if(!$from){
            return '';
        }else{
            $sql = "SELECT * ";
            $sql .= $from;
            $sql .= $this->buildWhere();
            $sql .= $this->buildOrder();
            $sql .= $this->buildLimit();

            return $sql;
        }
    }

    /**
     * @return string
     */
    private function buildWhere(): string
    {
        $sql = '';
        $where = $this->getWhere();

        if ($where) {
            if (is_array($where) && count($where) > 1) {
                foreach ($where as $index => $item) {
                    preg_match('/:\D+(\d+)?/', $item, $matches);
                    $var = $matches[0];
                    $str = preg_replace('/' . $var . '/', $this->getParam($var), $item);
                    if (0 == $index) {
                        $sql .= "WHERE $str ";
                    } else {
                        $sql .= "AND $str ";
                    }
                }
            } else {
                $item = $where[0];
                preg_match('/:\D+(\d+)?/', $item, $matches);
                $var = $matches[0];
                $str = preg_replace('/' . $var . '/', $this->getParam($var), $item);
                $sql .= "WHERE $str ";
            }
        }
        return $sql;
    }

    /**
     * @return string
     */
    private function buildOrder(): string
    {
        $sql = '';
        $order = $this->getOrder();

        if ($order) {
            $key = key($order);
            $sql .= "ORDER BY $key $order[$key] ";
        }
        return $sql;
    }

    /**
     * @return string
     */
    private function buildLimit(): string
    {
        $sql = '';
        $limit = $this->getLimit();

        if ($limit) {
            $sql .= "LIMIT $limit";
        }
        return $sql;
    }

    /**
     * @return string
     */
    private function buildFrom(): string
    {
        $sql = '';
        $from = $this->getFrom();
        if($from){
            $sql .= "FROM " . $from . ' ';
        }
        return $sql;
    }
}
