<?php

use App\core\Container;

$container = Container::getInstance();
try {
    $db = $container->get('db');
    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
} catch (Exception $e) {
    echo $e->getMessage();
    exit();
}

