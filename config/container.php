<?php

use App\core\Container;

$container = Container::getInstance();

$container->set('db', function (Container $container) {
    return new PDO('mysql:host=localhost;dbname=test','root');
});

return $container;