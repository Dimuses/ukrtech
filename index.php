<?php

require __DIR__ . "/vendor/autoload.php";
$container = require __DIR__ . "/config/container.php";
require  __DIR__ . "/config/bootstrap.php";

$query = new \App\Query();
$query
    ->from('table')
    ->where(['age > :age', 'salary = :salary'], [':age' => "value", ':salary' => 20])
    ->where('custom < :custom', [':custom' => 5])
    ->order('age')
    ->limit(10);

echo $query;